/*

App: Booking System API

Scenarion:
	A course booking system application where a user can enroll into a course

Type: Course Booking System (Web App)

Description: 
	A course booking system application where a user can error into a cours
	Allows an admin to do the CRUD operations
	Allows users to register into our database

Features:
	-User login(User Authentication)
	-User Registration

	Customer/Autenticated Users:
	-View Courses (all active courses)
	-Enroll course

	Admin Users:
	-Add Course
	-Update Course
	-Archive/Unarchive a course (soft delete /reactivate the course)
	-View courses (all courses active/inactive)
	-View/Manage User Accounts

	All Users(guests, customers, admin)
	-View Active Courses

*/

// Data Model for the Booking System



// Two-way embedding

/*

user {

	id - unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin:
	enrollments: [
		{
			id - document identifier
			course ID
			courseName - optional
			isPaid,
			dateEnrolled

		}

	]
}


course {
	id - unique for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrollees: [
		{
			id - document identifier
			userId,
			isPaid,
			dateEnrolled
		}
	]
}


*/


/* With Referencing
	user {
		id - unique identifier for the document
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin:
	}

	course {
		id - unique for the document
		name,
		description,
		price,
		slots,
		isActive,
		createdOn,
	}

	enrollment {
		id - unique for the document
		userId - the unique identifier for the user
		courseId - the unique identifier 
	}


*/

