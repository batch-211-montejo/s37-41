const mongoose = require("mongoose")

 const courseSchema = new mongoose.Schema({  
 	name: String,
	description: String,
	price: Number,
	slots: Number,
	isActive: Boolean,
	createdOn: Date,
	enrollees: [
		{
			userId: String,
			isPaid: Boolean,
			dateEnrolled: Date
		}
	]          
});

 module.exports = mongoose.model("Course", courseSchema);